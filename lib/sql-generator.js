/* eslint-disable object-shorthand */
const isNewEntry = (object, primaryKey) => {
    if (!primaryKey) { // no way to know if new or insert
        return true;
    }

    return typeof object[primaryKey] !== 'undefined' ? !Boolean(object[primaryKey]) : true; // eslint-disable-line
};

// TODO: Clean this function up
module.exports.saveSQL = (object, primaryKey, values) => {
    const newEntry = isNewEntry(object, primaryKey);
    const table = object.tableName;
    let sql = newEntry ? `INSERT INTO ${table} ` : `UPDATE ${table} SET `;

    const keys = Object.keys(values);
    const params = [];

    if (newEntry) {
        sql += '(';

        let start = true;
        keys.forEach((key) => {
            if (!start) {
                sql += ', ';
            }

            sql += `${key}`;
            start = false;
        });

        sql += ') values (';
    }

    let start = true;
    keys.forEach((key, idx) => {
        const index = idx + 1;

        params.push(object[key]);
        if (!start) {
            sql += ', ';
        }

        if (newEntry) {
            sql += `$${index}`;
        } else {
            sql += `${key} = $${index}`;
        }

        start = false;
    });

    if (newEntry) {
        sql += `) RETURNING ${primaryKey}`;
    } else {
        params.push(object[primaryKey]);
        const index = params.length;
        sql += ` WHERE ${primaryKey} = $${index}`;
    }

    return { sql: sql, params: params };
};

// TODO: Clean this function up
module.exports.findSQL = (object, values, limit, options) => {
    const table = object.tableName;
    const params = [];
    options = options || {}; // eslint-disable-line
    const select = typeof options.columns === 'undefined' ? '*' : options.columns.join(', ');
    let sql = `SELECT ${select} FROM ${table} `;
    const keys = Object.keys(values);

    if (typeof values !== 'undefined') {
        let start = true;
        keys.forEach((key) => {
            const index = params.length + 1;
            if (start) {
                sql += 'WHERE ';
            } else {
                sql += 'AND ';
            }

            if (values[key] === null) {
                sql += `${key} `;
            } else {
                sql += `${key} = $${index} `;
                params.push(values[key]);
            }

            start = false;
        });
    }

    if (typeof options.sort !== 'undefined') {
        const sort = options.sort.join(', ');
        sql += `ORDER BY ${sort} `;
    }

    if (limit) {
        sql += 'limit 1';
    }

    return { sql: sql, params: params }; // eslint-disable-line
};

module.exports.deleteSQL = (object) => {
    const table = object.tableName;
    const primaryKey = object.primaryKey; // eslint-disable-line
    const params = [object[primaryKey]];
    const sql = `DELETE FROM ${table} WHERE ${primaryKey} = $1`;

    return { sql: sql, params: params }; // eslint-disable-line
};
