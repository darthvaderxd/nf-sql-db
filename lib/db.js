const pg = require('pg');

const pool = new pg.Pool();
const Promise = require('bluebird');

module.exports.getDB = async () => {
    const db = {};

    db.query = async (sql, params) => {
        const client = await pool.connect();

        if (typeof process.env.DEBUG_QUERY !== 'undefined' && process.env.DEBUG_QUERY) {
            console.log('sql => ', sql);
            console.log('params => ', params);
        }

        return new Promise((resolve, reject) => {
            client.query(sql, params, (err, result) => {
                if (err) {
                    return reject(err);
                }

                client.release();
                resolve(result.rows || []);

                return result.rows || [];
            });
        });
    };

    db.close = () => {
        console.warn('close has been deprecated, please update your code');
    };

    return db;
};
