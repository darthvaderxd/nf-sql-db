/* eslint-disable no-param-reassign */
const sqlGenerator = require('./lib/sql-generator');
const Promise = require('bluebird');
const extend = require('util')._extend; // eslint-disable-line no-underscore-dangle
const DB = require('./lib/db');

let db = false;

const getDB = async () => {
    db = await DB.getDB();
};

const columnValueChanged = (object, column) => {
    if (typeof object.original === 'undefined' ||
        typeof object.original[column] === 'undefined' ||
        object.original[column] !== object[column]) {
        return true;
    }

    return false;
};

const getParams = (object, primaryKey) => {
    if (typeof (object.columns) === 'undefined') {
        throw new Error('Your object needs object.columns defined with an array of column names');
    }

    const params = {};

    object.columns.forEach((col) => {
        if (typeof object[col] !== 'undefined') {
            const different = columnValueChanged(object, col);
            if (!primaryKey || (different && col !== primaryKey && object[col] != null)) {
                params[col] = object[col];
            }
        }
    });

    return params;
};

const populateOriginal = (object) => {
    const original = {};
    object.columns.forEach((column) => {
        original[column] = object[column];
    });

    return original;
};

const generateObject = (object, data) => {
    const obj = extend({}, object);
    obj.columns.forEach((column) => {
        obj[column] = data[column];
    });

    obj.original = populateOriginal(obj);

    return obj;
};

module.exports.save = async (object) => {
    if (!db) {
        await getDB();
    }

    const primaryKey = typeof (object.primaryKey) !== 'undefined' && object.primaryKey
        ? object.primaryKey
        : false;

    const params = getParams(object, primaryKey);
    const result = sqlGenerator.saveSQL(object, primaryKey, params);

    return new Promise((resolve, reject) => {
        if (params.length === 0) {
            resolve(object);
        }

        db.query(result.sql, result.params)
            .then((rows) => {
                if (rows.length > 0) {
                    if (primaryKey) {
                        object[primaryKey] = rows[0][primaryKey];
                    }
                }

                object.original = populateOriginal(object);
                resolve(object);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

module.exports.findById = async (object, id) => {
    if (!db) {
        await getDB();
    }

    const primaryKey = typeof (object.primaryKey) !== 'undefined' && object.primaryKey
        ? object.primaryKey
        : false;

    if (!primaryKey) {
        throw new Error('Your object needs a primaryKey definition to findById');
    }

    const params = {};
    params[primaryKey] = id;

    const result = sqlGenerator.findSQL(object, params, true);

    return new Promise((resolve, reject) => {
        db.query(result.sql, result.params)
            .then((rows) => {
                if (rows.length > 0) {
                    const row = generateObject(object, rows[0]);
                    resolve(row);
                } else {
                    resolve(false);
                }
            })
            .catch((err) => {
                reject(err);
            });
    });
};

module.exports.find = async (object, params, options) => {
    if (!db) {
        await getDB();
    }

    options = options || {};
    const result = sqlGenerator.findSQL(object, params, false, options);
    return new Promise((resolve, reject) => {
        db.query(result.sql, result.params)
            .then((rows) => {
                if (rows.length > 0) {
                    rows.forEach((row, index) => {
                        const obj = generateObject(object, row);
                        rows[index] = obj;
                    });
                }

                resolve(rows);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

module.exports.delete = async (object) => {
    if (!db) {
        await getDB();
    }

    const primaryKey = typeof (object.primaryKey) !== 'undefined' && object.primaryKey
        ? object.primaryKey
        : false;

    if (!primaryKey) {
        throw new Error('Your object needs a primaryKey definition to delete the entry');
    }

    const result = sqlGenerator.deleteSQL(object);
    return new Promise((resolve, reject) => {
        db.query(result.sql, result.params)
            .then((data) => {
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
    });
};
