const db = require('./index');

module.exports = () => {
    const table = {
        tableName: 'users',
        primaryKey: 'user_id',
        columns: [
            'user_id',
            'username',
            'password',
            'email',
            'first_name',
            'last_name',
            'active',
            'last_login',
            'date_created',
            'reset_hash',
            'reset_hash_experation',
        ],
        type: {
            user_id: 'integer',
            username: 'text',
            password: 'hash',
            email: 'text',
            first_name: 'text',
            last_name: 'text',
            active: 'boolean',
            last_login: 'timestamp',
            date_created: 'timestamp',
            reset_hash: 'hash',
            reset_hash_experation: 'timestamp',
        },
        delete: () => db.delete(table),
        save: (object) => {
            if (typeof object === 'undefined') {
                return db.save(table);
            }

            return db.save(object);
        },
        findById: (id) => {
            if (id) {
                return db.findById(table, id);
            }

            return false;
        },
        find: (params, options) => {
            if (params) {
                return db.find(table, params, options);
            }

            return false;
        },
        original: {
            user_id: null,
            username: null,
            password: null,
            email: null,
            first_name: null,
            last_name: null,
            active: null,
            last_login: null,
            date_created: null,
            reset_hash: null,
            reset_hash_experation: null,
        },
        user_id: null,
        username: null,
        password: null,
        email: null,
        first_name: null,
        last_name: null,
        active: null,
        last_login: null,
        date_created: null,
        reset_hash: null,
        reset_hash_experation: null,
    };

    return table;
};
