/**
 * todo update this to a real unit test using jester or something
 */

const User = require('./example_table');

let user_id;

const createUserTest = async () => {
    console.log('Creating a user');

    const user = User();

    user.username = 'rtest';
    user.password = 'test';
    user.email = 'rtest@nilfactor.com';
    user.first_name = 'Richard';
    user.last_name = 'Test';
    user.active = true;
    user.last_login = null;
    user.date_created = new Date();

    try {
        console.log('\tsaving the user');
        await user.save();
        console.log('\tUsername: =>', user.username);

        user_id = user.user_id; // eslint-disable-line
    } catch (error) {
        console.log('createUserTest Error: ', error);
    }

    console.log('Save User Test Complete');

    return true;
};

const lookupUserAndModifyTest = async () => {
    console.log('Looking up user by id and modify');
    const user = User();

    try {
        console.log('\tLooking up user by id => ', user_id);
        const userLookup = await user.findById(user_id);
        console.log('\tUsername: => ', userLookup.username);

        console.log('\tModifying User');
        userLookup.first_name = 'Rich';
        await userLookup.save(userLookup);

        const checkModification = await user.findById(user_id);
        console.log('\tNew First Name => ', checkModification.first_name);
    } catch (error) {
        console.log('lookupUserTest Error: ', error);
    }

    console.log('Looking up user and modify test complete');
    return true;
};

const deleteUserTest = async () => {
    console.log('Delete user test');

    const user = User();

    try {
        const userLookup = await user.findById(user_id);
        console.log('\tDeleting first user');
        const deleteUserResult = await userLookup.delete(userLookup);
        console.log('\t', deleteUserResult);
    } catch (error) {
        console.log('lookupUserTest Error: ', error);
    }

    console.log('Delete user test complete');
    return true;
};

const runTests = async () => {
    await createUserTest();
    await lookupUserAndModifyTest();
    await deleteUserTest();
};

runTests();
